import Express from 'express'
import IndexRouter from './routes/index.js'

const app = new Express()
const port = process.env.PORT || 1337

app.set("view engine", "pug");

app.use('/', IndexRouter)

app.listen(port, () => {
  console.log(`The server was started on localhost:${port}`)
})