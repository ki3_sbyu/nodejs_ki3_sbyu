import fetch from 'node-fetch'
import config from '../config/config.json'

export default async function getWeather(city = '') {
  const { api, appid, units } = config
  const weather = await (await fetch(`${api}?appid=${appid}&q=${city}&units=${units || 'metric'}`)).json()

  return weather
}