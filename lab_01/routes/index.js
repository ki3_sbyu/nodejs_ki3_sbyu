import Express from 'express'
import getWeather from './weather.js'

const { Router } = Express
const router = new Router()

router.get('/', (req, res) => {
  res.render('pages/index')
})

router.get('/weather', async (req, res) => {
  const city = escape(req.query.city || '')

  if (req.query.city) {
    res.redirect(`/weather/${city}`)
  } else {
    res.render('pages/weather')
  }
})

router.get('/weather/:city', async (req, res) => {
  const city = escape(req.params.city || '')
  const weather = await getWeather(city)

  const data = {
    weather: weather.main,
    city: weather.name
  }
  
  res.render('pages/weather', data)
})

export default router