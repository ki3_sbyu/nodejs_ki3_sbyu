import Express from 'express'
import dbConnect from './db/mongo-connect.js'
import router from './routes/index.js'
import bodyParser from 'body-parser'

const app = new Express()
const port = process.env.PORT || 1337

app.use(bodyParser.urlencoded({ extended: false }))

app.set("view engine", "pug");
app.use(router)

dbConnect().then(() => {
  app.listen(port, () => {
    console.log(`The server was started on localhost:${port}`)
  })
})
