import mongoose from 'mongoose'
import validate from 'validator'

const { Schema, model } = mongoose
const userSchema = new Schema({
  login: {
    type: String,
    required: true
  },

  password: {
    type: String,
    required: true,
    trim: true,
    validate(value) {
      return validate.isLength(value, { min: 7 }) && value.trim() !== 'password'
    }
  },

  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    validate(value) {
      return validate.isEmail(value.trim())
    }
  }
})

const User = new model('user', userSchema)

export default User