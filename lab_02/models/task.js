import mongoose from 'mongoose'

const { Schema, model } = mongoose
const taskSchema = new Schema({
  description: {
    type: String,
    required: true,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  }
})

const Task = new model('task', taskSchema)

export default Task