import Express from 'express'
import userRouter from './user.js'
import taskRouter from './task.js'

const { Router } = Express
const mainRouter = new Router()

mainRouter.use(userRouter)
mainRouter.use(taskRouter)

export default mainRouter