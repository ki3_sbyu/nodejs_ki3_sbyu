import Express from 'express'
import User from '../models/user.js'

const { Router } = Express
const userRouter = new Router()

//get all users
userRouter.get('/api/users', async (req, res) => {
  try {
    const users = await User.find({})

    res.status(200).send(users)
  } catch {
    res.status(500).send('Error code: 500')
  }
})

//get by id
userRouter.get('/api/user', async (req, res) => {
  const id = req.query.id

  try {
    if (!id) {
      throw new Error()
    }

    const result = await User.findOne({ _id: id })

    res.send(result)
  } catch {
    res.send({ code: 404, message: 'Not found' })
  }
})

//create new user
userRouter.post('/api/user', async (req, res) => {
  try {
    const { login, password, email } = req.body
    const user = new User({ login, password, email })
    const result = await user.save()

    res.send(result)
  } catch {
    res.send({ message: 'Not valid data' })
  }
})

export default userRouter