import Express from 'express'
import Task from '../models/task.js'

const { Router } = Express
const taskRouter = new Router()

//get all tasks
taskRouter.get('/api/tasks', async (req, res) => {
  try {
    const tasks = await Task.find({})

    res.status(200).send(tasks)
  } catch(e) {
    res.status(500).send('Error code: 500')
  }
})

//get by id
taskRouter.get('/api/task', async (req, res) => {
  const id = req.query.id

  try {
    if (!id) {
      throw new Error()
    }

    const result = await Task.findOne({ _id: id })

    res.send(result)
  } catch {
    res.send({ code: 404, message: 'Not found' })
  }
})

//create new task
taskRouter.post('/api/task', async (req, res) => {
  try {
    const { description, completed } = req.body
    const task = new Task({ description, completed })
    const result = await task.save()

    res.send(result)
  } catch {
    res.send({message: 'Not valid data'})
  }
  
})

export default taskRouter