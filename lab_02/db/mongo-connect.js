import mongoose from 'mongoose'
import config from '../config/config.json'

export default function connect() {
  return mongoose.connect(`${config.mongodb}/${config.dbname}`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
}