import mongoose from 'mongoose'
import config from '../config/config.json'

const dbConnection = mongoose.connect(`${config.dbURI}/${config.dbName}`, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
})

export default dbConnection