import Express from 'express'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import cookieSession from 'cookie-session'
import dbConnection from './db/dbConnect.js'
import router from './routes/index.js'


const port = process.env.PORT || 1337
const app = new Express()

app.use(bodyParser.urlencoded({ extended: true }))

app.use('/', router)

dbConnection.then(() => {
  app.listen(port, () => console.log(`Server was started on localhost:${port}`))

})