import Express from 'express'
import apiRouter from './api.js'
import authRouter from './auth/auth.js'

const mainRouter = new Express.Router()

mainRouter.use('/api', apiRouter)
mainRouter.use('/', authRouter)

export default mainRouter