import Express from 'express'
import userApi from './api/user.js'
import taskApi from './api/task.js'

const apiRouter = new Express.Router()

apiRouter.use('/user', userApi)
apiRouter.use('/task', taskApi)

export default apiRouter