import Express from 'express'
import User from '../../models/user.js'
import auth from '../../middleware/auth.js'

const { Router } = Express
const authRouter = new Router()

authRouter.get('/user/me', auth, (req, res) => {
  res.send(req.user)
})

authRouter.post('/user/login', async (req, res) => {
  try {
    const { email, password } = req.body
    const user = await User.findOneByCredentials(email, password)
    const token = await user.generateAuthTokens()

    res.status(200).send({ user, token })
  } catch(e) {
    res.status(400).send(e)
  }
})

authRouter.post('/user/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter(token => token.token !== req.token)

    await req.user.save()

    res.status(200).send()
  } catch(e) {
    res.status(500).send(e)
  }
})

authRouter.post('/user/logoutAll', auth, async (req, res) => {
  try {
    req.user.tokens = []

    const user = await req.user.save()

    res.status(200).send()
  } catch(e) {
    res.status(500).send(e)
  }
})

export default authRouter