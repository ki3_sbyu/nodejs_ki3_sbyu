import Express from 'express'
import auth from '../../middleware/auth.js'
import { populateTasks } from '../../middleware/task.js'
import Task, { taskSchema } from '../../models/task.js'

const { Router } = Express

const taskRouter = new Router()
const taskProps = Object.keys(taskSchema.obj)

taskRouter.post('/', auth, async (req, res) => {
  try {
    const task = new Task({
      ...req.body,
      owner: req.user.id
    })

    await task.save()

    res.status(201).send(task)
  } catch(e) {
    res.status(500).send(e)
  }
})

taskRouter.get('/', auth, populateTasks, async (req, res, next) => {
  if (req.query.id) {
    const { id } = req.query

    try {
      const result = req.user.tasks.filter(task => task._id == id)

      if (result.length) {
        res.status(200).send(result.pop())
      } else {
        res.status(404).send('Not Found')
      }

    } catch(e) {
      res.status(500).send(e)
    }
  } else {
    next()
  }
})

taskRouter.get('/', auth, populateTasks, async (req, res) => {
  try {
    res.status(200).send(req.user.tasks)
  } catch(e) {

    res.status(500).send(e)
  }
})

taskRouter.put('/', auth, populateTasks, async (req, res) => {
  try {
    console.log(req.body)
    const task = await Task.findById(req.body.id)

    if (task.owner == req.user.id) {
      taskProps.forEach(prop => {
        if (req.body[prop]) {
          task[prop] = req.body[prop]
        }
      })

      await task.save()

      res.status(200).send(task)
    } else {
      res.status(404).send('Not Found')
    }

  } catch(e) {
    res.status(500).send(e)
  }
})

export default taskRouter