import Express from 'express'
import User, { userSchema } from '../../models/user.js'
import auth from '../../middleware/auth.js'

const userRouter = new Express.Router()
const userProps = Object.keys(userSchema.obj)

userRouter.get('/', async (req, res) => {
  const { query } = req
  const result = await User.find(query)

  res.status(200).send(result)
})

userRouter.put('/', auth, async (req, res) => {
  try {
    userProps.forEach(prop => {
      if (req.body[prop]) {
        req.user[prop] = req.body[prop]
      }
    })

    const result = await req.user.save()

    res.status(200).send(result)
  } catch(e) {
    res.status(400).send(e)
  }
})

userRouter.post('/', async (req, res) => {
  try {
    const user = new User(userProps.reduce((obj, key) => {
      obj[key] = req.body[key]
  
      return obj
    }, {}))
    
    res.status(200).send(await user.save())
  } catch(e) {
    res.status(400).send(e)
  }
})

userRouter.delete('/me', auth, async (req, res) => {
  try {
    await req.user.remove()

    res.status(200).send()
  } catch(e) {
    res.status(500).send(e)
  }
}) 

export default userRouter