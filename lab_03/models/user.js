import mongoose from 'mongoose'
import validator from 'validator'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import config from '../config/config.json'

const { model, Schema } = mongoose
const { isEmail, isLength } = validator

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    validate(value) {
      return isLength(value, { min: 3, max: 64 })
    },
  },

  password: {
    type: String,
    required: true,
    trim: true,
  },

  age: {
    type: Number,
    required: false,
    validate(value) {
      return +value && value > 0 && value < 150
    },
  },

  email: {
    type: String,
    required: false,
    unique: true,
    trim: true,
    validate(value) {
      return isEmail(value)
    },
  },
  
  tokens: [{
    token: {
      type: String,
      required: true
    }
  }]
})

userSchema.pre('save', async function (next) {
  try {
    if (this.isModified('password')) {
      this.password = await bcrypt.hash(this.password, +config.bcryptRounds)
    }
  } catch(e) {
    next(e)
  } finally {
    next()
  }
})

userSchema.statics = {
  ...userSchema.statics,

  async findOneByCredentials(email, password) {
    const user = await userModel.findOne({ email: email })

    if (!user) {
      throw new Error('Incorrect email')
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
      throw new Error('Incorrect password')
    }

    return user
  }
}

userSchema.methods = {
  ...userSchema.methods,

  async generateAuthTokens() {
    const token = jwt.sign({ _id: this._id.toString() }, config.tokenSecret)

    this.tokens.push({ token })

    await this.save()

    return token
  },

  toJSON() {
    const userObject = this.toObject()

    delete userObject.password
    delete userObject.tokens;

    return userObject
  }
}

userSchema.virtual('tasks', {
  ref: 'task',
  localField: '_id',
  foreignField: 'owner'
})

var userModel = new model('user', userSchema)

export default userModel
export {
  userSchema
}