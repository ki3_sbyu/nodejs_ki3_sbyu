import mongoose from 'mongoose'

const { model, Schema } = mongoose

const taskSchema = new Schema({
  description: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  owner: {
    type: mongoose.Types.ObjectId,
    required: true,
    ref: 'user'
  }
})

const taskModel = new model('task', taskSchema)

export default taskModel
export {
  taskModel,
  taskSchema
}