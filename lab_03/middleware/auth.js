import jwt from 'jsonwebtoken'
import config from '../config/config.json'
import User from '../models/user.js'

async function auth(req, res, next) {
  try {
    const token = req.header('Authorization')
    const decoded = jwt.verify(token, config.tokenSecret)
    const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })

    if (!user) {
      throw new Error('User is not found')
    }

    req.user = user
    req.token = token

    next()

  } catch(e) {
    res.status(401).send({ ...e, message: 'Please authenticate' })
  }
}

export default auth

export {
  auth
}