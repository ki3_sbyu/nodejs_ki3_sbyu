
async function populateTasks(req, res, next) {
  try {
    await req.user.populate('tasks').execPopulate()

    next()
  } catch(e) {
    res.status(500).send({ ...e })
  }
}

export {
  populateTasks
}